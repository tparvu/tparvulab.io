---
layout: default
---

<head>
  <base target="_blank">
</head>

# Ted Parvu's Curriculum Vitae
Open Source, Privacy & Security Advocate, Wildlife Defender, DAO Advocate.

<div id="webaddress">
<a href="tel:+1-218-235-4303">+1-218-235-4303</a> | <a href="mailto:ted@tedstechshack.com">ted@tedstechshack.com</a> | <a href="https://github.com/tparvu" target="_blank">GitHub</a> | <a href="https://tparvu.github.io" target="_blank">tparvu.github.io</a> | <a href="https://keybase.io/ted_parvu" target="_blank">keybase.io</a> | <a href="https://medium.com/@ted_parvu" target="_blank">Medium</a> | <a href="https://www.linkedin.com/in/tedparvu/" target="_blank">LinkedIn</a>
</div>

## About Me

My fascination with computing started in 1981 when my parent's gave me a Sinclair ZX-81.
I attended college at the University of Arizona where I was privileged to work with world-renowned optical scientists and astronomers.
Over time I became an Open Source, Privacy, and Security advocate.
For as long as I can remember, I have been in love with the natural world.
That love has grown into a passion to protect the environment.
I have worked in computing, information security, control systems, artificial intelligence, outfitting, bear wrangling, and most recently, in blockchain.

## Employment

`July 2021 - present`
[__Inspire11__](https://inspire11.com) &mdash; Chicago office, remote (Ely, Minnesota)<br>
_Technical Consultant_

Inspire11 is a full service, digital and management consulting firm dedicated to making an impact for our clients, employees and communities. 
We believe that Change and Disruption are guaranteed. 
Evolution and relevancy are the keys to building sustainable organizations that will serve generations to come.

Technical consultant specializing in security, blockchain, DevOps, CI/CD, AWS Infrastucture, developer workflow.

__DevOps - Fortune 500 logistics and manufacturing company__
Transformed a critical production application by thoroughly analyzing and refactoring the AWS cloud environment, developer workflows, release management, QA processes, and security protocols.

Collaborated with cross-functional teams, including developers, product management, and architects, to devise a solution for a legacy cloud-based application. 
Architected and implemented a migration to AWS Organizations, AWS Identity Center, and GitLab pipelines with custom private runners.

Successfully onboarded and facilitated the handover of foundational Infrastructure as Code and environments to developers, architects, and DevOps engineers. 
The redesigned environment prioritized maintainability, security, efficiency, robustness, and ease of use.

Implemented comprehensive pipelines, encompassing Static Application Security, Secret Detection, artifact management, build processes, deployment, unit testing, and more.

Developed and implemented policies, procedures, and technical guidelines for seamless code promotion across development, QA, and production stages.

Overall, orchestrated a transformation from an ad-hoc legacy software development cycle to an Agile, secure, and efficient best practices approach—from local developer environments to production deployment and operations.

__DevOps – Fortune 500 logistics and manufacturing company__
Rapid development of a proof of concept customer portal.
Identified initial AWS setup, investigated AWS managed services, setup GitLab accounts and environments. 
Implemented GitLab CI/CD pipelines, container services, and setup of AWS App Runner managed service. 
Setup pipeline secrets management. 
Private GitLab runners and pools. 
Worked with developers to create a DevOps work flow.

__DevOps - Nationwide chiropractic network__
Minimum viable product development for corporation's first cloud application. 
Initial DevOps lift in Azure DevOps. 
Setup CI/CD pipelines, secrets management, container registries, build process, and deployment. 
Setup Azure Kubernetes Service to host application.
Worked with developers on Kubernetes. 
Worked closely with Architect to bring vision to life whilst keeping everything secure and reliable.

__DevOps – Fortune 500 financial institution__
Investigated abandoned DevOps infrastructure responsible for running a production application. After exhaustive research and trial and error, recommended refactoring the infrastructure while working with the development team. 
Architected and implemented AWS Aurora Database multi-region clusters. Wrote IaC to migrate current database to multi-region redundant clusters. This was to meet disaster recovery needs.
Worked with Gitlab pipelines, CI/CD runners, and large teams of developers, admins, and stake holders.

__Business Development__
Worked with multiple teams, as DevOps SME, to build proposals.
Pursued potential clients in Blockchain space.

`February 2021 - May 2021`
[__Titan__](https://titan.io) &mdash; Fully distributed team (Ely, Minnesota)<br>
_DevOps Engineering Lead_

Titan is a leading developer of software and services for advancing Bitcoin mining at scale.

Responsible for AWS infrastructure design, operations, maintenance, security, CI/CD, for Titan Bitcoin mining pool.

Provided configuration management, security, public key infrastructure, automated maintenance, monitoring, alarms, infrastructure as code, for AWS and Linux environments.

Managed GitLab repositories, Google Workspace, enterprise access management, AWS, bare metal servers, Slack, VPNs.

`September 2017 - March 2020`
[__Bloq__](https://bloq.com) &mdash; Fully distributed team (Ely, Minnesota)<br>
_Senior Information Security Engineer_

Bloq[^1] provides managed blockchain products, enterprise support, and development of new blockchain and cryptocurrency technology.

At Bloq I performed in many different roles as business needs arose.

I architected the infrastructure for Metronome,[^2] the world's first autonomous cryptocurrency.[^3]

I Worked with our development and design teams in our move to an Agile development model.

I designed, deployed, maintained and monitored our developers' microservices and infrastructure on Amazon Web Services (AWS).
I also maintained, monitored, and secured our AWS development and production environments.

Role as cryptography, encryption, secrets management, security, bug bounty, subject matter expert.
Role as IT, system administration, public key infrastructure, DevOps, DevSecOps, design and management.

Evaluate, recommend, and manage collaborative services for our fully distributed team.

Collaborated with development teams to design, implement, and manage CI/CD pipelines from staging to production.

Developed and supported Bloq Enterprise software, packages, security and distribution.

Designed, implemented and maintained cryptocurrency hot and cold wallet infrastructure and security.

Worked with Ethereum, Bitcoin, Geth, Parity, OpenEthereum, Ethereum Classic, and other blockchains and related software and services.

`November 2014 - December 2019`
[__Ted's Tech Shack__](https://tedstechshack.com) &mdash; Ely, Minnesota surrounding area and remote<br>
_Technology Consultant_

Ted's Tech Shack[^4] was my technology consulting company serving the Iron Range area of northern Minnesota.

I met with local business owners to design and implement technology to meet their needs.
Projects varied from video surveillance, networking, printers, point of sale, web sites, content management, inventory control, and more.

I also worked on various online freelance projects from around the world.

`November 2011 - September 2014`
[__North American Bear Center__](https://bear.org) &mdash; Ely, Minnesota<br>
[__Wildlife Research Institute__](https://bearstudy.org)<br>
_Field Technician_<br>
_Research Assistant_

The North American Bear Center[^5] and the Wildlife Research Institute[^6] are sister organizations dedicated to advancement of the long-term survival of bears and other wildlife around the world.

I improved on the early stages of a remote, solar powered, cellular connected, live streaming, remote wilderness, wild bear den camera system.[^7] [^8]

I worked with world renowned bear researchers helping to maintain tracking and monitoring equipment on non-tranquilized wild bears.[^9]

`January 2012 - May 2012`
__British Broadcasting Corporation [(BBC)](https://www.bbc.co.uk/aboutthebbc/)__ &mdash; Ely, Minnesota<br>
_Bear Wrangler_<br>
_Fixer_<br>
_Production Assistant_

The British Broadcasting Corporation (BBC) is the world's oldest national broadcaster.[^10]

I worked with camera and production crews, presenters, and other crew as a wilderness guide and bear wrangler.

Role as fixer and liaison between BBC crew and local area businesses and citizenry.

Assisted film and production crews with Planet Earth Live worldwide broadcasts.[^11]

`May 2008 - October 2011`
[__Canadian Waters__](https://www.canadianwaters.com/) &mdash;  Ely Minnesota<br>
_Outfitter_
_Guide_

Canadian Waters is an outfitter serving the Boundary Waters Canoe Area and Quetico wildernesses.

I outfitted and advised clients on gear, hazards, routes, fishing, activities, and other information pertaining to their trips in the BWCAW and Quetico wildernesses.

I also performed duties related to retail sales, including inventory, registers, point of sale, networking and computer support.

`July 2007 - October 2007`
[__Piragis Northwoods Company__](https://www.piragis.com/) &mdash;  Ely Minnesota<br>
_Outfitter_

Piragis Northwoods Company is an outfitter serving the Boundary Waters Canoe Area and Quetico wildernesses.

Outfitted and trained clients in use of gear such as canoes, tents, stoves and more.

Maintained, repaired, and inspected gear such as canoes and tents.

`November 2006 - January 2007`
__Casbah Tea House__ &mdash;  Tucson Arizona<br>
_Cook_

The Casbah Teahouse was an eco-friendly, organic, vegetarian restaurant, coffee & tea house.

I worked as a cook and in other aspects of food preparation and general restaurant work.

`April 2005 - March 2006`
__Teaching Drum Outdoor School__ &mdash;  Three Lakes, Wisconsin<br>
_Staff_

The Teaching Drum is a wilderness immersion outdoor school.

I managed the school office and setup, maintained, and documented various business and technology systems.

I assisted in various day to day aspects of running the school and also worked as a article and book editor.

`December 2000 - September 2001`
__Network Associates, PGP Security division__ &mdash;  Santa Clara, California<br>
_Senior Software Engineer_

Network Associates was formed in 1997 as a merger of McAfee Associates, Network General, PGP Corporation and Helix Software.

I was the primary software engineer on the PGP E-Business Server product.
The E-Business server was a public key infrastructure and encryption product that was ported to multiple platforms (AIX, HP-UX, Linux, Solaris, Windows NT).
I developed a thread-safe API for the PGP E-Business Server.
I worked with release engineering to enable the porting and building to multiple platforms.

Built and managed numerous UNIX development and build systems.

Assisted in training of QA team in UNIX environments.

`October 1999 - April 2000`
__Virtual Space Network__ &mdash;  Phoenix, Arizona<br>
_Development Manager_

VSNI was a small but rapidly growing hosting and internet provider.

I managed a distributed team of programmers, customer support, and system administrators on multiple projects.

Project design and implementation for an automated web hosting site.

Security and operations.

`April 1999 - October 1999`
__Institute for Bioengineering__ &mdash;  Phoenix, Arizona<br>
_Consultant_

The Institute for Bioengineering was a private research group.

I managed a security audit of the networks and computer systems.
Identified and then implemented the most critical aspects of the security audit.

Consulted, identified and then implemented collaboration tools such as email, web tools, shared calendars, and content management.

`April 1999 - October 1999`
__Numatico__ &mdash;  Phoenix, Arizona<br>
_Contractor_

Numatico was a private defense contractor.

Software engineering development of distributed back-end for US Naval helm control system.
System integration between naval control system and third party Wonderware Factory Suite system.

`October 1998 - April 1999`
__NDC Health Information Services__ &mdash;  Phoenix, Arizona<br>
_Lead Engineer_

NDC HIS was a large data warehouse for confidential medical records.

Developed and documented corporate UNIX security policies and procedures.

Large UNIX & NT server administration.

Implemented firewalls, IDS, scanners, and other security tools.

Member of team of database administrators, on what was at the time, the largest Oracle instance in the world at 3 Terabytes.

`July 1997 - May 1998`
__Motorola Space & Systems Technology Group__ &mdash;  Chandler, Arizona<br>
_Principal Engineer_

Division of Motorola that designed the Iridium satellite network.[^12]

Aided in design of test and integration plans for Iridium Gateways.

Integrated complex vendor subsystems into Iridium Gateways.

Administered Windows NT, UNIX, vendor systems and networks.

Designed and implemented test beds.

`April 1994 - April 1997`
__ADS Communications__ &mdash; Phoenix, Arizona<br>
_Lead Software Engineer_

ADS Communications was a provider of service automation and dispatch software for the office equipment industry.

Engineering design, implementation, programming, and integration of Automated Dispatch System.
This included interprocess communications on SCO UNIX TCP/IP stack and integration of complex real-time systems.

System administration of development and production UNIX systems.

Back-line customer support and relations with end users.

`December 1993 - April 1994`
__Hughes Missile Systems__ &mdash; Tucson, Arizona<br>
_Programmer/Analyst_

Hughes Missile Systems was part of the Hughes Aircraft Company and has since become Raytheon Missiles & Defense.

Development, systems analysis, and maintenance of factory production shop floor control and management software.

Analyze and maintain computer systems.
Train shop floor personnel in use of systems and software.

`June 1989 - December 1993`
[__Steward Observatory__](https://mirrorlab.arizona.edu/) &mdash; Tucson, Arizona<br>
_Staff Programmer_
_Student Worker_

The Steward Observatory Mirror Lab is now known as the Richard F. Caris Mirror Laboratory where a team of scientists and engineers are making giant, lightweight mirrors of unprecedented power for a new generation of optical telescopes.

I began as a student worker helping to monitor the critical process of casting large mirrors.
Between castings I transitioned to supporting the lab's network and computer systems.
I then began working with research scientists to bring their theories into practical engineering applications.

Full cycle design and development of control system used for writing precision holograms.
Developed control systems for precision metrology of large optics.
Programmed data acquisition systems for large optics tests.
Assisted in analysis of test data and system quality.
Member of team that in 1992 developed the largest, single cast, telescope mirror at 6.5 meters f1.25.

`December 1986 - August 1991`
__CIMSoft__ &mdash;  Glendale, Arizona<br>
_Co-founder_<br>
_Programmer/Analyst_

CIMSoft was a company I founded with two classmates in High School.
Despite our inexperience in life and business we designed, marketed, and maintained a medical records chart tracking system at Thunderbird Samaritan Hospital.

I was the developer of a custom software system written in Turbo C that far outperformed other commercial systems at the time.
Our team worked with the end users and managers to tailor a system that met their specific needs.

Team consulted with small businesses and individuals advising on software, and hardware.

I developed custom software for small business such as a chemical labeling system.

## Volunteer Work

`2008 - 2013`
__Ely Area Food Shelf__ &mdash;  Ely, Minnesota

Local area food shelf working to end hunger through community partnerships.

Our family assisted with unloading, stocking, and coordinating monthly food delivery and volunteers.

Coordinated with local chapter of Cub Scouts for food drive.

`2008 - 2010`
__Voyageurs Area Council Cub Scouts__ &mdash;  Ely, Minnesota

Our family participated in the local area Boy Scouts council.
This included organizing and participating in activities, workshops, banquets, food drives, etc.

I created web site with forums and calendar for local scouting organization and gave public presentation and written instructions to pack members on use of technology.

`2004 - 2005`
[__Black Pine Animal Sanctuary__](https://www.bpsanctuary.org/) &mdash;  Albion, Indiana

Black Pine provides refuge to captive-raised exotic animals and educates the public about responsible animal care and conservation.

Volunteered assistance in the care and feeding of exotic animals such as lions, tigers, and bears.

Helped maintain facilities and enclosures.

Guided tour groups and worked with outreach programs to help educated the public.


## Professional Interests

Encryption, public key encryption, blockchain, security and privacy.

UNIX, Linux, Open Source, programming and administration.

Real time systems, control systems, complex systems, systems engineering.

Research and development.

Electronics, hardware, circuits, custom solutions.

GPS, telemetry, wireless, cellular, solar, wind, hydro power and systems.

Surveillance and wildlife video and audio.

## Sample of tools known

* Git, GitHub, CVS
* Linux, UNIX, RedHat, CentOS, Debian, Ubuntu, Arch, Solaris, SunOS, AIX, HP-UX, FreeBSD, SCO UNIX, NetBSD, OpenBSD
* DOS, Windows, macOS
* Apache, SQL, MongoDB, Redis, ScyllaDB
* VMware, VirtualBox, Docker, DockerHub, LXD, LXC, Travis
* SSH, GnuPG, PGP, Nmap, OpenSSL, OpenVPN
* Bash, C, Pascal, TCP/IP, Sockets, WebSockets
* Blockchain, Bitcoin, Geth, Parity, Ethereum
* Amazon Web Services (AWS), DigitalOcean, PaperTrail, LogicMonitor, DataDog
* Signal, Zendesk, Slack, Zoom, GSuite

## Personal Interests

Audiophile, computer audio, recording, streaming, mastering.

Gardening, permaculture, rewilding, foraging, tracking, Traditional Ways, crafts and skills.

Consensus decision making, social systems, egalitarian societies, intentional communities.

## Computer Skills

I possess very strong skills in UNIX C programming, UNIX system administration, and computer security.

I am experienced in systems integration, engineering, and testing.

I possess a variety of related skills such as web administration, Microsoft and Apple software and systems. Networking, database administration, customer support, software and systems analysis, technical documentation.

I am able to leverage my critical thinking skills to quickly assess, analyze, and learn new systems, software, and methodologies.

## Education

`August 1993`
__UNIVERSITY OF ARIZONA__ &mdash;  Tucson, Arizona<br>
_Bachelor of Arts, Interdisciplinary Studies_

My degree is a tailored program with three fields of study instead of the standard major/minor program.
The specific fields of study are Computer Science, Mathematics, and Philosophy with a focus on Artificial Intelligence.

## Footnotes

[^1]: [https://bloq.com](https://bloq.com)
[^2]: [https://metronome.io](https://metronome.io)
[^3]: [https://github.com/autonomoussoftware](https://github.com/autonomoussoftware)
[^4]: [https://tedstechshack.com](https://tedstechshack.com)
[^5]: [https://bear.org](https://bear.org)
[^6]: [https://bearstudy.org](https://bearstudy.org)
[^7]: [Lily's Den Shed is Up](https://www.bearstudy.org/website/updates/daily-updates/1820-lilys-den-shed-is-up-update-november-24-2012-.html)
[^8]: [We're Online!](https://www.bearstudy.org/website/updates/daily-updates/1502-were-online-.html)
[^9]: [Changing Lily’s GPS Batteries](https://www.bearstudy.org/website/updates/daily-updates/1659-changing-lilys-gps-batteries-update-june-12-2012.html)
[^10]: [https://www.bbc.co.uk/aboutthebbc](https://www.bbc.co.uk/aboutthebbc)
[^11]: [Planet Earth Live](https://www.bbc.co.uk/programmes/b01ms18l)
[^12]: [Iridium satellite constellation](https://en.wikipedia.org/wiki/Iridium_satellite_constellation)
