---
layout: default
title: Ted Parvu - Computer Security Specialist
---
# Ted Parvu - Computer Security Specialist

<div id="webaddress">
  ted@tedstechshack.com | Mobile: +1-218-235-4303
</div>

## Summary

With over 20 years of professional experience, I possess a wide breadth of skills and knowledge that enable me to perceive how complex systems behave and interact.
From the fundamentals of TCP/IP programming to security situational awareness, I have the experience to understand complex systems and their interactions.
This awareness enables me to see vulnerabilities and attack surfaces others may overlook.

## Sample Work Experience

`February 2021 - May 2021`
[__Titan Industries__](https://titan.io) &mdash; Fully distributed team (Ely, Minnesota)<br>
_DevOps Engineering Lead_

Titan is a leading developer of software and services for advancing Bitcoin mining at scale.

Responsible for AWS, operations, maintenance, security, for Titan Bitcoin mining pool.

Provided configuration management, security, public key infrastructure, automated maintenance, monitoring, alarms, infrastructure as code, for AWS and Linux environments.

Managed GitLab repositories, Google Workspace, enterprise access management, AWS, bare metal servers, Slack, VPNs.

`September 2017 - March 2020`
[__Bloq__](https://bloq.com){:target="_blank"} &mdash; Fully distributed team (Ely, Minnesota)\\
_Senior Information Security Engineer_

Bloq provides managed blockchain products, enterprise support, and development of new blockchain and cryptocurrency technology.

At Bloq I performed in many different roles as business needs arose.

I architected the infrastructure for Metronome, the world's first autonomous cryptocurrency.

I Worked with our development and design teams in our move to an Agile development model.

I designed, deployed, maintained and monitored our developers' microservices and infrastructure on Amazon Web Services (AWS).
I also maintained, monitored, and secured our AWS development and production environments.

Role as cryptography, encryption, secrets management, security, bug bounty, subject matter expert.
Role as IT, system administration, public key infrastructure, DevOps, DevSecOps, design and management.

Evaluate, recommend, and manage collaborative services for our fully distributed team.

Collaborated with development teams to design, implement, and manage CI/CD pipelines from staging to production.

Developed and supported Bloq Enterprise software, packages, security and distribution.

Designed, implemented and maintained cryptocurrency hot and cold wallet infrastructure and security.

Worked with Ethereum, Bitcoin, Geth, Parity, OpenEthereum, Ethereum Classic, and other blockchains and related software and services.

`November 2014 - December 2019`
[__Ted's Tech Shack__](https://tedstechshack.com){:target="_blank"} &mdash; Ely, Minnesota surrounding area and remote\\
_Technology Consultant_

Ted's Tech Shack was my technology consulting company serving the Iron Range area of northern Minnesota.

I met with local business owners to design and implement technology to meet their needs.
Projects varied from video surveillance, networking, printers, point of sale, web sites, content management, inventory control, and more.

I also worked on various online freelance projects from around the world.

`November 2011 - September 2014`
[__North American Bear Center__](https://bear.org){:target="_blank"} &mdash; Ely, Minnesota\\
[__Wildlife Research Institute__](https://bearstudy.org){:target="_blank"}\\
_Field Technician_\\
_Research Assistant_

The North American Bear Center and the Wildlife Research Institute are sister organizations dedicated to advancement of the long-term survival of bears and other wildlife around the world.

I improved on the early stages of a remote, solar powered, cellular connected, live streaming, remote wilderness, wild bear den camera system.

I worked with world renowned bear researchers helping to maintain tracking and monitoring equipment on non-tranquilized wild bears.

`January 2012 - May 2012`
__British Broadcasting Corporation [(BBC)](https://www.bbc.co.uk/aboutthebbc/){:target="_blank"}__ &mdash; Ely, Minnesota\\
_Bear Wrangler_\\
_Fixer_\\
_Production Assistant_

The British Broadcasting Corporation (BBC) is the world's oldest national broadcaster.

I worked with camera and production crews, presenters, and other crew as a wilderness guide and bear wrangler.

Role as fixer and liaison between BBC crew and local area businesses and citizenry.

Assisted film and production crews with Planet Earth Live worldwide broadcasts.

`December 2000 - September 2001`
__Network Associates, PGP Security division__ &mdash;  Santa Clara, California\\
_Senior Software Engineer_

Network Associates was formed in 1997 as a merger of McAfee Associates, Network General, PGP Corporation and Helix Software.

I was the primary software engineer on the PGP E-Business Server product.
The E-Business server was a public key infrastructure and encryption product that was ported to multiple platforms (AIX, HP-UX, Linux, Solaris, Windows NT).
I developed a thread-safe API for the PGP E-Business Server.
I worked with release engineering to enable the porting and building to multiple platforms.

Built and managed numerous UNIX development and build systems.

Assisted in training of QA team in UNIX environments.

`October 1998 - April 1999`
__NDC Health Information Services__ &mdash;  Phoenix, Arizona\\
_Lead Engineer_

NDC HIS was a large data warehouse for confidential medical records.

Developed and documented corporate UNIX security policies and procedures.

Large UNIX & NT server administration.

Implemented firewalls, IDS, scanners, and other security tools.

Member of team of database administrators, on what was at the time, the largest Oracle instance in the world at 3 Terabytes.

`June 1989 - December 1993`
[__Steward Observatory__](https://mirrorlab.arizona.edu/){:target="_blank"} &mdash; Tucson, Arizona\\
_Staff Programmer_
_Student Worker_

The Steward Observatory Mirror Lab is now known as the Richard F. Caris Mirror Laboratory where a team of scientists and engineers are making giant, lightweight mirrors of unprecedented power for a new generation of optical telescopes.

I began as a student worker helping to monitor the critical process of casting large mirrors.
Between castings I transitioned to supporting the lab's network and computer systems.
I then began working with research scientists to bring their theories into practical engineering applications.

Full cycle design and development of control system used for writing precision holograms.
Developed control systems for precision metrology of large optics.
Programmed data acquisition systems for large optics tests.
Assisted in analysis of test data and system quality.
Member of team that in 1992 developed the largest, single cast, telescope mirror at 6.5 meters f1.25.

## Sample of tools known

* Git, GitHub, GitLab, CVS
* Ansible, Terraform, Docker, Travis
* pfSense, IPFire, UFW, iptables
* Linux, UNIX, RedHat, CentOS, Debian, Ubuntu, Arch, Solaris, SunOS, AIX, HP-UX, FreeBSD, SCO UNIX, NetBSD, OpenBSD
* DOS, Windows, macOS
* Apache, SQL, MongoDB, Redis, ScyllaDB
* VMware, VirtualBox, Docker, DockerHub, LXD, LXC
* SSH, GnuPG, PGP, Nmap, OpenSSL, OpenVPN
* Golang, Bash, C, Pascal, TCP/IP, Sockets, WebSockets
* Blockchain, Bitcoin, Geth, Parity, Ethereum
* Amazon Web Services (AWS), DigitalOcean, PaperTrail, LogicMonitor, DataDog
* Signal, Zendesk, Slack, Zoom, Google Workspace

## Education

`August 1993`
__UNIVERSITY OF ARIZONA__ &mdash;  Tucson, Arizona\\
_Bachelor of Arts, Interdisciplinary Studies_

My degree is a tailored program with three fields of study instead of the standard major/minor program.
The specific fields of study are Computer Science, Mathematics, and Philosophy with a focus on Artificial Intelligence.
