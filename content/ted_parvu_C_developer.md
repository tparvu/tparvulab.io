---
layout: default
---

<head>
  <base target="_blank">
</head>
  
# Ted Parvu - Senior C Developer

<div id="webaddress">
  ted@tedstechshack.com | Mobile: +1-218-235-4303
</div>

## Summary

With over 20 years of professional experience, I possess a wide breadth of skills and knowledge that enable me to perceive how complex systems behave and interact.
From the fundamentals of TCP/IP programming to security situational awareness, I have the experience to understand complex systems and their interactions.

## Sample Work Experience

`February 2021 - May 2021`
[__Titan Industries__](https://titan.io) &mdash; Fully distributed team (Ely, Minnesota)<br>
_DevOps Engineering Lead_

Titan is a leading developer of software and services for advancing Bitcoin mining at scale.

Responsible for AWS, operations, maintenance, security, for Titan Bitcoin mining pool.

Provided configuration management, security, public key infrastructure, automated maintenance, monitoring, alarms, infrastructure as code, for AWS and Linux environments.

Managed GitLab repositories, Google Workspace, enterprise access management, AWS, bare metal servers, Slack, VPNs.

`September 2017 - March 2020`
[__Bloq__](https://bloq.com)  &mdash; Fully distributed team (Ely, Minnesota)\\
_Senior Information Security Engineer_

Bloq provides managed blockchain products, enterprise support, and development of new blockchain and cryptocurrency technology.

At Bloq I performed in many different roles as business needs arose.

I architected the infrastructure for Metronome, the world's first autonomous cryptocurrency.

I Worked with our development and design teams in our move to an Agile development model.

I designed, deployed, maintained and monitored our developers' microservices and infrastructure on Amazon Web Services (AWS).
I also maintained, monitored, and secured our AWS development and production environments.

Role as cryptography, encryption, secrets management, security, bug bounty, subject matter expert.
Role as IT, system administration, public key infrastructure, DevOps, DevSecOps, design and management. 

Evaluate, recommend, and manage collaborative services for our fully distributed team.

Collaborated with development teams to design, implement, and manage CI/CD pipelines from staging to production.

Developed and supported Bloq Enterprise software, packages, security and distribution.

Designed, implemented and maintained cryptocurrency hot and cold wallet infrastructure and security.

Worked with Ethereum, Bitcoin, Geth, Parity, OpenEthereum, Ethereum Classic, and other blockchains and related software and services.

`November 2014 - December 2019`
[__Ted's Tech Shack__](https://tedstechshack.com)  &mdash; Ely, Minnesota surrounding area and remote\\
_Technology Consultant_

Ted's Tech Shack was my technology consulting company serving the Iron Range area of northern Minnesota. 

I met with local business owners to design and implement technology to meet their needs.
Projects varied from video surveillance, networking, printers, point of sale, web sites, content management, inventory control, and more.

I also worked on various online freelance projects from around the world.

`November 2011 - September 2014`
[__North American Bear Center__](https://bear.org)  &mdash; Ely, Minnesota\\
[__Wildlife Research Institute__](https://bearstudy.org) \\
_Field Technician_\\
_Research Assistant_ 

The North American Bear Center and the Wildlife Research Institute are sister organizations dedicated to advancement of the long-term survival of bears and other wildlife around the world. 

I improved on the early stages of a remote, solar powered, cellular connected, live streaming, remote wilderness, wild bear den camera system.

I worked with world renowned bear researchers helping to maintain tracking and monitoring equipment on non-tranquilized wild bears.

`December 2000 - September 2001`
__Network Associates, PGP Security division__ &mdash;  Santa Clara, California\\
_Senior Software Engineer_

Network Associates was formed in 1997 as a merger of McAfee Associates, Network General, PGP Corporation and Helix Software. 

I was the primary software engineer on the PGP E-Business Server product. 
The E-Business server was a public key infrastructure and encryption product that was ported to multiple platforms (AIX, HP-UX, Linux, Solaris, Windows NT).
I developed a thread-safe API for the PGP E-Business Server.
I worked with release engineering to enable the porting and building to multiple platforms.

Built and managed numerous UNIX development and build systems. 

Assisted in training of QA team in UNIX environments. 

`October 1999 - April 2000`
__Virtual Space Network__ &mdash;  Phoenix, Arizona\\
_Development Manager_

VSNI was a small but rapidly growing hosting and internet provider. 

I managed a distributed team of programmers, customer support, and system administrators on multiple projects.

Project design and implementation for an automated web hosting site. 

Security and operations.

`April 1999 - October 1999` 
__Institute for Bioengineering__ &mdash;  Phoenix, Arizona\\
_Consultant_

The Institute for Bioengineering was a private research group. 

I managed a security audit of the networks and computer systems. 
Identified and then implemented the most critical aspects of the security audit.

Consulted, identified and then implemented collaboration tools such as email, web tools, shared calendars, and content management.

`April 1999 - October 1999`
__Numatico__ &mdash;  Phoenix, Arizona\\
_Contractor_

Numatico was a private defense contractor. 

Software engineering development of distributed back-end for US Naval helm control system.
System integration between naval control system and third party Wonderware Factory Suite system.

`October 1998 - April 1999`
__NDC Health Information Services__ &mdash;  Phoenix, Arizona\\
_Lead Engineer_

NDC HIS was a large data warehouse for confidential medical records.

Developed and documented corporate UNIX security policies and procedures.

Large UNIX & NT server administration.

Implemented firewalls, IDS, scanners, and other security tools.

Member of team of database administrators, on what was at the time, the largest Oracle instance in the world at 3 Terabytes.

`July 1997 - May 1998`
__Motorola Space & Systems Technology Group__ &mdash;  Chandler, Arizona\\
_Principal Engineer_

Division of Motorola that designed the Iridium satellite network.[^12]

Aided in design of test and integration plans for Iridium Gateways.

Integrated complex vendor subsystems into Iridium Gateways.

Administered Windows NT, UNIX, vendor systems and networks.

Designed and implemented test beds.

`April 1994 - April 1997`
__ADS Communications__ &mdash; Phoenix, Arizona\\
_Lead Software Engineer_

ADS Communications was a provider of service automation and dispatch software for the office equipment industry.

Engineering design, implementation, programming, and integration of Automated Dispatch System.
This included interprocess communications on SCO UNIX TCP/IP stack and integration of complex real-time systems.

System administration of development and production UNIX systems.

Back-line customer support and relations with end users.

`June 1989 - December 1993`
[__Steward Observatory__](https://mirrorlab.arizona.edu/)  &mdash; Tucson, Arizona\\
_Staff Programmer_
_Student Worker_

The Steward Observatory Mirror Lab is now known as the Richard F. Caris Mirror Laboratory where a team of scientists and engineers are making giant, lightweight mirrors of unprecedented power for a new generation of optical telescopes.

I began as a student worker helping to monitor the critical process of casting large mirrors. 
Between castings I transitioned to supporting the lab's network and computer systems.
I then began working with research scientists to bring their theories into practical engineering applications.

Full cycle design and development of control system used for writing precision holograms.
Developed control systems for precision metrology of large optics.
Programmed data acquisition systems for large optics tests.
Assisted in analysis of test data and system quality.
Member of team that in 1992 developed the largest, single cast, telescope mirror at 6.5 meters f1.25.

`December 1986 - August 1991`
__CIMSoft__ &mdash;  Glendale, Arizona\\
_Co-founder_\\
_Programmer/Analyst_

CIMSoft was a company I founded with two classmates in High School. 
Despite our inexperience in life and business we designed, marketed, and maintained a medical records chart tracking system at Thunderbird Samaritan Hospital.

I was the developer of a custom software system written in Turbo C that far outperformed other commercial systems at the time.
Our team worked with the end users and managers to tailor a system that met their specific needs.

Team consulted with small businesses and individuals advising on software, and hardware.

I developed custom software for small business such as a chemical labeling system. 


## Sample of tools known

* Git, GitHub, GitLab, CVS
* Ansible, Terraform, Docker, Travis
* pfSense, IPFire, UFW, iptables
* Linux, UNIX, RedHat, CentOS, Debian, Ubuntu, Arch, Solaris, SunOS, AIX, HP-UX, FreeBSD, SCO UNIX, NetBSD, OpenBSD
* DOS, Windows, macOS
* Apache, SQL, MongoDB, Redis, ScyllaDB
* VMware, VirtualBox, Docker, DockerHub, LXD, LXC
* SSH, GnuPG, PGP, Nmap, OpenSSL, OpenVPN
* Golang, Bash, C, Pascal, TCP/IP, Sockets, WebSockets
* Blockchain, Bitcoin, Geth, Parity, Ethereum
* Amazon Web Services (AWS), DigitalOcean, PaperTrail, LogicMonitor, DataDog
* Signal, Zendesk, Slack, Zoom, Google Workspace
    
## Education

`August 1993`
__UNIVERSITY OF ARIZONA__ &mdash;  Tucson, Arizona\\
_Bachelor of Arts, Interdisciplinary Studies_

My degree is a tailored program with three fields of study instead of the standard major/minor program.
The specific fields of study are Computer Science, Mathematics, and Philosophy with a focus on Artificial Intelligence.
