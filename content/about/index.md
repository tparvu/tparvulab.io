+++
title = "About"
+++

<h1>About</h1>

My fascination with computing started in 1981 when my parent's gave me a Sinclair ZX-81.
I attended college at the University of Arizona where I was privileged to work with world-renowned optical scientists and astronomers.
Over time I became an Open Source, Privacy, and Security advocate.
For as long as I can remember, I have been in love with the natural world.
That love has grown into a passion to defend the environment.
Some areas I have worked in are; computing, information security, control systems, artificial intelligence, outfitting, bear wrangling, and most recently, in blockchain technology.
